const{createApp} = Vue;

createApp({
    data(){
        return{
            username:"",
            password:"",
            error: null,
            sucesso: null,
            userAdmin: false,

            //Variáveis para armazenamento das entradas do formulario da Pagina de cadastro
            newUsername:"",
            newpassword:"",
            confirmPassword:"",

            //Declaração dos arrys para armazenamento de usuarios e senhas
            usuarios: ["Admin" , "Abda" , "Ana"],
            senhas: ["123452023" , "123456" , "0509200"],
            mostrarEntrada: false,
            mostrarLista: false,
        }
    },//Fechamento Data

    methods:{
        login(){
            setTimeout(() => {
                // alert("Dentro do setTimeout");
                if((this.username === "Abda"&& this.password === "123452023") ||
                (this.username === "Ana"&& this.password === "12345") ||
                (this.username === "Admin"&& this.password ==="admin12345")){
                    this.sucesso ="Login efetuado com sucesso!";
                    this.error = null;
                    if(this.username === "Admin"){
                        this.userAdmin = true;
                    }
                }
                else{
                    // alert("Login não efetuado!");
                    this.error = "Nome ou senha incorretos!";
                    this.sucesso = null;
                }
            },1000);
            // alert("Saiu do setTimeouty!!!");
        },//Fechamento Login

        login2(){
            this.mostrarEntrada = false;

            this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
            this.senhas = JSON.parse(localStorage.getItem("senhas"));

            setTimeout(() => {
                this.mostrarEntrada = true;
                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);
                    this.error = null;
                    this.sucesso = "Login efetuado com sucesso";
                    if(this.username === "Admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!!!";
                    } 
                }//Fechamento if
                else{
                    this.error = "Nome ou senha incorretos!";
                    this.sucesso = null;
                }//Fechament else

                this.username = "";
            this.password = "";
                
            }, 500);
            
        },//Fechamento login2

        paginaCadastro(){
            this.mostrarEntrada = false;
            if(this.userAdmin == true){
                this.sucesso = "Login ADM identificado!";
                this.error = null;
                this.mostrarEntrada = true;
                
                // localStorage.setItem("username", this.username);
                // localStorage.setItem("password", this.password);

                setTimeout(() => {}, 2000);
                
                setTimeout(() => {
                    window.location.href = "paginaCadastro.html";

                },1000)//Fechamento setTimeout
               
            }
            else{
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.error = "Faça login com usuario administrativo";
                }, 500);//Fechamento setTimeout
                
            }//Fechamento else
        },//Fechamento painaCadastro

        adicionarUsuario(){
            this.username = localStorage.getItem("username");
            this.password = localStorage.getItem("password");

            this.mostrarEntrada = false;

            setTimeout(() => {
                this.mostrarEntrada = true;
                if(this.username === "Admin"){
                    if(this.newUsername !== ""){
                        if(!this.usuarios.includes(this.newUsername)){
                           if(this.newpassword && this.newpassword == this.confirmPassword){
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newpassword);
                            //Armazenamento do usuário e senha no LocalStorage

                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));

                            this.error = null;
                            this.sucesso = "Usuário cadastrado com sucesso";
                        }//Fechamento do if
                        else{
                            this.sucesso = null;
                            this.error ="Senha incorreta!!!";
                        }
                        }//Fechamento do includes
                        else{
                            this.sucesso = null;
                            this.error = "O usuário informado já está cadastrado!";
                        }
                    }//Fechamento if !== ""
                    else{
                        this.error = "Por favor,digite um nome de usuario";
                        this.sucesso = null;
                    }//Fechamento do else        
                
                  }//Fechamento if
                  else{
                    this.error = "Usuario NÃO ADM!!!";
                    this.sucesso = null;
                  }//Fechamento else

                  this.newUsername = "";
                  this.newpassword = "";
                  this.confirmPassword = "";
            }, 500);   //Fechamento setTimeout       
        },//Fechamento adicionarUsuario

        verCadastrados(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            this.mostrarLista = !this.mostrarLista;
        },//Fechament verCadastrados

        excluirUsuário(usuario){
            this.mostrarEntrada = false;
            if(usuario === "Admin"){
                //Impedir a exclusão do usuário admin
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.error = "O usuário ADMIN não pode ser excluído!!!";
                }, 500);
                return;//Força a finalização do bloco / Função
            }//Fechamento if usuario

            if(confirm("Tem certeza que deseja excluir o usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);
                    //Atualização dos vetores no localStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));

                }//Fechamento index
            }//Fechamento do if confirm
        },//Fechamento excluirUsuário
    },//Fechamento methods

}).mount("#app"); 