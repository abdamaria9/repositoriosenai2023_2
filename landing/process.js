const {createApp} = Vue;

createApp({
    data(){
        return{
            products:[
                {
                    id: 1,
                    name: "Tênis",
                    description: "Um par de tênis confortável para esportes",
                    price: 129.99,
                    image: "./imagens/imgTenis.jpg",
                },//Fechamento item 1
                {
                    id: 2,
                    name: "Botas",
                    description: "Botas elegantes para qualquer ocasião",
                    price: 199.99,
                    image: "./imagens/imgBotas.jpg",
                },//Fechamento item 2
                {
                    id: 3,
                    name: "Sapatos",
                    description: "Sapatos clássicos para um visual sofisticado!",
                    price: 149.99,
                    image: "./imagens/imgSapatos.jpg",
                },//Fechamento item 3
                {
                    id: 4,
                    name: "Sandálias",
                    description: "Sandálias confortaveis para os pés!",
                    price: 69.99,
                    image: "./imagens/imgSandalias.jpg",
                },//Fechamento item 4
            ],//Fechamento products
            currentProduct: {}, //produto atual
            cart: [],
            
        };//Fechamento return
    },//Fechamento data

    mounted(){
        window.addEventListener("hashchange", this.updateProduct);
        this.updateProduct();

    },//Fechamento mounted

    methods:{
        updateProduct(){
            const productId = window.location.hash.split("/")[2];
            const product = this.products.find(item => item.id.toString() === productId);
            this.currentProduct = product ? product : {};

        },//Fechamento updateProduct

        addToCart(product){
            this.cart.push(product);
        }
    },//Fechamento methods
}).mount("#app"); //Fechamento createApp